// NO 3 //
console.log('No 3');
function sum(startNum, finishNum, step) {
    let a = startNum;
    let b = finishNum;
    let c = step;
    let d = 0;
    let checkStep = c === undefined ? 1 : c;
    let createFunc = (n) => {return n};
        if(a < b) {
            for(i = a; i <= b; i+=checkStep){
                d += i;
                createFunc(d);
            }
            return d;
        } else if(a > b) {
            for(i = a; i >= b; i-=checkStep){
                d += i;
                createFunc(d);
            }
            return d;
        } else if(a === undefined && b === undefined && c === undefined) {
            return 0;
        } else if(b === undefined && c === undefined) {
            return 1;
        }
}
console.log(sum(1, 10));
console.log('==============');
console.log(sum(5, 50, 2));
console.log('==============');
console.log(sum(15, 10));
console.log('==============');
console.log(sum(20, 10, 2));
console.log('==============');
console.log(sum(1));
console.log('==============');
console.log(sum());
