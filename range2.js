// NO 2 //

function rangeWithStep(startNum, finishNum, Step){
    let a = startNum;
    let b = finishNum;
    let c = Step;
    let d = 0;
    let Num = [];
    let checkStep = c === undefined ? 1 : c;
    let createFunc = (n) => {return n};
    if(a<b) {
        for(i=a; i <= b; i+=checkStep){
            d += i;
            Num.push(i);
            createFunc(d);

        }
        return Num;
    }else if (a>b){
        for(i=a; i>=b; i-=checkStep){
            d += i;
            Num.push(i);
            Num.sort(function i (a, b, c) { return c-b-a});
            createFunc(d);
        }
        return Num;
    }   
}


console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]